import './App.css';
import './NavBar.css';
import { useState, useEffect} from 'react';
import axios from 'axios';
import NavBar from './NavBar';
import Footer from './Footer';
// import {
//   BrowserRouter as Router,
//   Routes, Route, Link
// } from "react-router-dom"

function App() {
  const [currentUser, setCurrentUser] = useState('Guest'); // current user. Guest default.
  const [users, setUsers] = useState([]); //database of users
  const [books, setBooks] = useState([]); //database of books. Tämä on myös navbarissa

  useEffect(() => { //setup users variable from server.
    console.log('Fetching data...');
    axios
      .get('http://localhost:3001/users/')
      .then(response => {
        console.log('Promise fulfilled');
        setUsers(response.data);
      });
  }, []);

  useEffect(() => { //setup books variable from server. Tämä on myös navbarissa
    console.log('Fetching data...');
    axios
      .get('http://localhost:3001/books/')
      .then(response => {
        console.log('Promise fulfilled');
        setBooks(response.data);
      });
  }, []);

 
  return (
    <div className="App">
      <header className="App-header">
      </header>
      <NavBar
        curUser={currentUser}
        setCurUser={setCurrentUser}
        users={users}
        books={books}
        setBooks={setBooks}
        setUsers={setUsers}
      />
      <Footer />
    </div>
  );
}

export default App;
