import { BrowserRouter as Router, Routes, Link, 
  Route, useMatch, useResolvedPath } from "react-router-dom";
import Signup from "./Signup";
import Frontpage from "./Frontpage";
import Booksearch from "./Booksearch";
import LoginForm from "./Login";
import Logout from "./Logout";
import ListOfBooks from "./ListOfBooks";
import MyPage from "./MyPage";

const NavBar = ({curUser, setCurUser, users, setUsers, books, setBooks}) => {

  return (
    <Router>
      <nav className="nav">
        <Link to="/">
        <img src="https://trello.com/1/cards/63ca7ace84da49014f9f027e/attachments/63ce35296a321c0313501042/download/ver.png"
         style={{height: "40px"}} alt="logo"/></Link>
        <ul>
          <CustomLink to="/"> Home </CustomLink>
          <CustomLink to="/books"> Books </CustomLink>
          <CustomLink to="/booksearch"> Search </CustomLink>
          {curUser === 'Guest' && <>
          <CustomLink to="/signup"> Signup </CustomLink>
          <CustomLink to="/login"> Login </CustomLink>
            </>}
          {curUser !== 'Guest' && <>
          <CustomLink to="/mypage"> Profile </CustomLink>
          <CustomLink to="/logout"> Logout </CustomLink>
            </>}
        </ul>
      </nav>
      <Routes>
        <Route path="/" element={<Frontpage/>} />
        <Route path="/booksearch" element={<Booksearch curUser={curUser} books={books} setBooks={setBooks} users={users} setUsers={setUsers}/>} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/books" element={<ListOfBooks books={books} />} />
        <Route path="/login" element={<LoginForm users={users} curUser={curUser} setCurUser={setCurUser}/>} />
        <Route path="/mypage" element={<MyPage users={users} setUsers={setUsers} curUser={curUser} books={books} setBooks={setBooks}/>} />
        <Route path="/logout" element={<Logout curUser={curUser} setCurUser={setCurUser}/>} />
      </Routes>
    </Router>
  )
}

function CustomLink({ to, children, ...props }) {
  const resolvedPath = useResolvedPath(to)
  const isActive = useMatch({ path: resolvedPath.pathname, end: true })

  return (
    <li className={isActive ? "active" : ""}>
      <Link to={to} {...props}>
        {children}
      </Link>
    </li>
    
  )
}

export default NavBar;