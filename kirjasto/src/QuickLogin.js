import { useState } from 'react';
//
// WIP in integration. works only as standalone atm.
//
function QuickLogin({curUser, setCurUser, users, isModalOpen, setIsModalOpen}) {
    const [tmpUser, setTmpUser] = useState('');
    const [tmpPw, setTmpPw] = useState('');
    function pwCheck() { //check if tmpUser matches any user from users. then check if user's pw match tmpPw. then set user as curUser
        const userIndex = users.findIndex(tmpUser);
            if (userIndex === -1) { // -1 indicates no match found
                console.log('No user found');
                return;
            }
        if (users[userIndex].password === tmpPw) {
            setCurUser(users[userIndex].name);
            setIsModalOpen(false);
            console.log('Login successful');
        }
        if (users[userIndex].password !== tmpPw) {
            console.log('Invalid password');
        }
    }
    if (curUser === 'Guest' || curUser === undefined) {
    return (
        <div>
            {isModalOpen && (
                <div className="modal">
                    <h2>Sign in</h2>
                    <form>
                        <label>Username: </label>
                        <input
                            type='text'
                            name='username'
                            onChange={e => setTmpUser(e.target.value)}
                        /><br />
                        <label>Password: </label>
                        <input
                            type='password'
                            name="password"
                            onChange={e => setTmpPw(e.target.value)}
                        /><br />
                        <button onClick={pwCheck}>Submit</button>
                    </form>
                    <button onClick={() => setIsModalOpen(false)}>Close</button>
                </div>
            )}
            {curUser === 'Guest' || curUser === undefined ? (
                <button onClick={() => setIsModalOpen(true)}>Open Login</button>
            ) : (
                <p>Welcome, {curUser}</p>
            )}
        </div>
    )}
}

export default QuickLogin;