import Book from "./Book";
import { useState, useEffect } from "react";
        
const ListOfBooks = ({books}) => {
    const [showBook, setShowBook] = useState(false);
    return (
        <div id="container4">
            <div className="bookListBox">
                {books.map(book => {
                    return (
                        <div id="boxForBook">
                            <p>{book.author}: {book.title}</p>
                        </div>
                    )
                })}
            </div>
        </div>

    );
}

export default ListOfBooks;