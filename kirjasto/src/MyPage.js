import React, { useState } from 'react';
import Book from './Book';

function MyPage({ curUser, users, books, setBooks, setUsers }) {
  
  
  const [message, setMessage] = useState("");
  const [borrowedBookState, setBorrowedBookState] = useState("");
  let curUserId = 0;
  let borrowedBooks = [];
  const curUserObj = users.find(user => user.name === curUser )
  console.log(curUserObj);
  let curUserBookHistory = [];
  if(curUserObj === undefined) {
    curUserBookHistory = [];
    curUserId = 0;
  }
  else {
    curUserBookHistory = curUserObj.books_history;
    curUserId = curUserObj.id;
  }
  console.log(curUserBookHistory);
  //curUserId = curUserObj.id;
  //console.log("curUserId" + curUserId);
  
  let bookids = books.map(book => book.id);
  let title = books.map(title => title.title);
  let titleFound = "";
  let index = 0;  
  for(let i = 0; i < bookids.length; i++) {
    index = curUserBookHistory[i];
    let copies = books[i].copies;

    for(let j = 0; j < copies.length; j++) {
      let idFound = parseInt(copies[j].borrower_id);
      console.log("idFound: " + idFound);
      console.log("curUserId: " + curUserId);
      
      if(idFound === curUserId) {
        borrowedBooks.push(books[i].title);
        console.log(borrowedBooks);
        console.log("testausta");
      }
    }
      
  } 
  
//setBorrowedBookState(borrowedBooks);

      
     const handleReturn = (e) => {
     e.preventdefault();
        setMessage("Books returned successfully!");
    };

  const myBooks = books.filter(book => {
    const myCopies = book.copies.filter(copy => copy.borrower_id === curUserId);
    if (myCopies.length === 0) {
      return false;
    }
    else {
      return true;
    }
  });

  return (
    <div id='container' className="success">
      <h1>Hello {curUser}!</h1>
      <h3>Welcome to the Blue library. <br/>
        {(myBooks.length === 0) ? <> You currently don't have any books.</> : <>You have borrowed these books:</>}
      </h3>
      <div id='borrowed-books_mypage'> {console.log(borrowedBooks)}
        {myBooks.map(book => {
          const myCopies = book.copies.filter(copy => copy.borrower_id === curUserId);
          const myBook = {...book, copies : myCopies};
          return (<Book curUser={curUser} books={books} book={myBook} setBooks={setBooks} users={users} setUsers={setUsers}/>)
        })
        }
      </div>
      {/* <button onClick={handleReturn} className="btn">
        Return
      </button>
      {message && <p></p>} */}
    </div>
  );
}

export default MyPage;